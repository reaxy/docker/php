FROM php:8.1-apache-bullseye
LABEL maintainer="Jean Soares Fernandes <3454862+jeansf@users.noreply.github.com>"

# Setup timezone
ENV TZ=America/Sao_Paulo

# Update sources
# Install "Git" – https://git-scm.com/
# Install Midnight Commander, Vim, Nano
# Install "Composer" – https://getcomposer.org/
# Install "ImageMagick" executable – https://www.imagemagick.org/script/index.php
# Install PHP Dependencies
RUN apt-get update -y && \
    apt-get install -y git mc vim nano imagemagick ca-certificates zlib1g-dev libicu-dev g++ libcurl4-openssl-dev libxslt-dev \
    libexif-dev libpq-dev libsqlite3-dev libmemcached-dev libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
    libfreetype6-dev libzip-dev librabbitmq-dev libmagic-dev && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


# Install PHP "curl" extension – http://php.net/manual/en/book.curl.php
# Install PHP "intl" extension – http://php.net/manual/en/book.intl.php
# Install PHP "xsl" extension – http://php.net/manual/en/book.xsl.php
# Install PHP "exif" extension – http://php.net/manual/en/book.exif.php
# Install PHP "mysqli" extension – http://php.net/manual/pl/book.mysqli.php
# Install PHP "pdo" extension with "mysql", "pgsql", "sqlite" drivers – http://php.net/manual/pl/book.pdo.php
# Install PHP "opcache" extension – http://php.net/manual/en/book.opcache.php
# Install PHP "memcached" extension – http://php.net/manual/en/book.memcached.php
# Install PHP "gd" extension
# Install PHP "zip" extension
# Install PHP "socket" extension
# Install PHP "amqp" extension
# Install PHP "iconv" extension
# Install PHP "ctype" extension
# Install PHP calendar
RUN docker-php-ext-install curl && \
    docker-php-ext-configure intl  && docker-php-ext-install intl && \
    docker-php-ext-install xsl && \
    docker-php-ext-install exif && \
    docker-php-ext-install mysqli && \
    docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql && \
    docker-php-ext-install pdo pdo_mysql pgsql pdo_pgsql pdo_sqlite && \
    docker-php-ext-install opcache && \
    pecl install memcached && docker-php-ext-enable memcached && \
    docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp --with-xpm && docker-php-ext-install gd && \
    docker-php-ext-install zip && \
    docker-php-ext-install sockets && \
    pecl install amqp-1.11.0beta && docker-php-ext-enable amqp && \
    docker-php-ext-install iconv && \
    docker-php-ext-install ctype && \
    docker-php-ext-install calendar && \
    docker-php-ext-install fileinfo && \
    rm -rf /var/lib/apt/lists/* /tmp/*

# Create default with wildcard servername
# Enable "mod_rewrite" – http://httpd.apache.org/docs/current/mod/mod_rewrite.html
# Enable "mod_headers" – http://httpd.apache.org/docs/current/mod/mod_headers.html
# Enable "mod_expires" – http://httpd.apache.org/docs/current/mod/mod_expires.html
# Remove default config apache2
# Remove warning
# Install configure PHP curl ssl
COPY symfony.conf /etc/apache2/sites-available/symfony.conf
RUN curl -L --remote-name --time-cond cacert.pem https://curl.se/ca/cacert.pem && mv cacert.pem /etc/ssl/certs/cacert.pem && \
    mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
    sed -i 's/^.*curl.cainfo.*$/curl.cainfo = \"\/etc\/ssl\/certs\/cacert.pem\"/' /usr/local/etc/php/php.ini && \
    sed -i "s/upload_max_filesize = .*/upload_max_filesize = 256M/" /usr/local/etc/php/php.ini && \
    sed -i "s/post_max_size = .*/post_max_size = 256M/" /usr/local/etc/php/php.ini && \
    sed -i "s/memory_limit = .*/memory_limit = 512M/" /usr/local/etc/php/php.ini && \
    sed -i 's/variables_order = .*/variables_order = "EGPCS"/' /usr/local/etc/php/php.ini && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && \
    echo "date.timezone=$TZ" >> /usr/local/etc/php/conf.d/default.ini && \
    sed -i 's/#AddDefaultCharset UTF-8/AddDefaultCharset UTF-8/g'  /etc/apache2/conf-enabled/charset.conf && \
    a2enmod rewrite && \
    a2enmod headers && \
    a2enmod expires && \
    a2enmod remoteip && \
    a2dissite 000-default && \
    a2ensite symfony && \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf
